/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CarpetInterface;

/**
 *
 * @author kelsey.pritsker676
 */
public class RoomCarpet {
    private RoomDimension size;
    private double carpetCost;
    
    public RoomCarpet(){
        this.size = new RoomDimension();
        this.carpetCost = 2.00;
    }
    
    public RoomCarpet(RoomDimension dim, double cost){
        this.size = dim;
        this.carpetCost = cost;
    }
    
    public double getTotalCost(){
        double sqrFt = size.getArea();
        double totalCost = sqrFt*carpetCost;
        return totalCost;
    }
    
    public String toString(){
        return("Total cost for carpeting of room with deminsions " 
                + size.toString() + "  at $" + carpetCost + 
                "/sqft: $" + getTotalCost() + "\n");
    }
}
